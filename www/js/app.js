// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

// for SQLite persistence
// https://www.thepolyglotdeveloper.com/2014/11/use-sqlite-instead-local-storage-ionic-framework/
// http://nextflow.in.th/en/2015/easy-way-to-work-with-sqlite-database-in-ionic-framework/
// http://ngcordova.com/docs/install/
var db;
var starter = angular.module('starter', ['ionic', 'ngCordova'])

.run(function($ionicPlatform, $cordovaSQLite) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    var db = window.sqlitePlugin.openDatabase({ name: 'my.db', location: 'default' }, function (db) {

      // Here, you might create or open the table.

    }, function (error) {
      console.log('Open database ERROR: ' + JSON.stringify(error));
    });
    /* $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Messages (id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
      'firstName TEXT, lastName TEXT, phoneNumber TEXT)'); */

  });
})

starter.config(function($stateProvider) {
  $stateProvider
    .state('index', {
      url: '/',
      templateUrl: 'index.html',
      controller: 'start'
    })
    .state('imagequestion', {
      url: '/imagequestion',
      templateUrl: 'imagequestion.html',
      params: {'imageQuestionAnswer': null, 'textQuestionAnswer': null},
      controller: 'imageQuestionCtrl'
    })
    .state('textquestion', {
      url: '/textquestion',
      templateUrl: 'textquestion.html',
      params: {'imageQuestionAnswer': null, 'textQuestionAnswer': null},
      controller: 'textQuestionCtrl'
    })
    .state('done', {
      url: '/done',
      templateUrl: 'done.html',
      params: {'imageQuestionAnswer': null, 'textQuestionAnswer': null},
      controller: 'doneCtrl'
    });
})

starter.controller('start', function($scope, $ionicModal, $ionicLoading, $ionicViewSwitcher, $state) {
  $scope.onStart = function () {
    $ionicViewSwitcher.nextDirection('forward');
    $state.go("imagequestion", {
      'imageQuestionAnswer': 0,
      'textQuestionAnswer': ""
    });
  };
})

starter.controller('imageQuestionCtrl', function($scope, $ionicModal, $ionicLoading, $ionicViewSwitcher, $state, $stateParams) {
  $scope.onNext = function (answer) {
    console.log("imageQuestionAnswer = "+answer);
    $ionicViewSwitcher.nextDirection('forward');
    $state.go("textquestion", {
      'imageQuestionAnswer': answer,
      'textQuestionAnswer': ""
    });
  };
})

starter.controller('textQuestionCtrl', function($scope, $ionicModal, $ionicLoading, $ionicViewSwitcher, $state, $stateParams) {
  $scope.onNext = function () {
    console.log("textQuestionAnswer = "+$scope.year);
    $ionicViewSwitcher.nextDirection('forward');
    $state.go("done", {
      'imageQuestionAnswer': $stateParams.imageQuestionAnswer,
      'textQuestionAnswer': $scope.year
    });
    $scope.year = null;
  };
})

starter.controller('doneCtrl', function($scope, $ionicModal, $ionicLoading, $ionicViewSwitcher, $state, $stateParams) {
  $scope.imageQuestionAnswer = $stateParams.imageQuestionAnswer;
  $scope.textQuestionAnswer = $stateParams.textQuestionAnswer;
  if (angular.equals($scope.imageQuestionAnswer, 35) && angular.equals($scope.textQuestionAnswer, "2016")) {
    $scope.score = "You got 2/2 correct!";
  } else if ((!angular.equals($scope.imageQuestionAnswer, 35) && angular.equals($scope.textQuestionAnswer, "2016"))
    || (angular.equals($scope.imageQuestionAnswer, 35) && !angular.equals($scope.textQuestionAnswer, "2016"))) {
    $scope.score = "You got 1/2 correct!";
  } else {
    $scope.score = "You got 0/2 correct :(";
  }

  console.log("textQuestionAnswer = "+$scope.imageQuestionAnswer+", imageQuestionAnswer = "+$scope.textQuestionAnswer);
  /*$scope.$on("$ionicView.beforeEnter", function(event, data){
    // handle event
    if (angular.equals($scope.imageQuestionAnswer, 35) && angular.equals($scope.textQuestionAnswer, "2016")) {
      $scope.score = "You got 2/2 correct!";
    } else if (!angular.equals($scope.imageQuestionAnswer, 35) && angular.equals($scope.textQuestionAnswer, "2016")
              || angular.equals($scope.imageQuestionAnswer, 35) && !angular.equals($scope.textQuestionAnswer, "2016")) {
      $scope.score = "You got 1/2 correct!";
    } else {
      $scope.score = "You got 0/2 correct :(";
    }
  }); */
  $scope.onDone = function () {
    $ionicViewSwitcher.nextDirection('back');
    $state.go("index");
  };
});
